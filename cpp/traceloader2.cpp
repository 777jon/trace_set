/*
 *  traceloader2.cpp
 *
 *  Author:         Elizabeth Krenkel
 *  Date Modified:  August 11, 2015 by Jazmin Ortiz
 *                  June 3, 2016 by Jonathan Cruz
 *
 *  Description:    A modified traceloader function, which does the same thing
 *                  as traceloader.cpp but takes in arguments from the
 *                  command line.
 */

#include <string>
#include <iostream>
#include <fstream>

#include "TraceSet.hpp"

using namespace std;

int main(int argc, char* argv[])
{
    string file_to_load;
    string freqLBASeq;
    bool calcInitial;
    for (int i = 1; i < argc; ++i) {
        string arg = argv[i];
        if (i + 1 != argc) { // Make sure we aren't at teh end of argv!
            if (arg == "-t") {
                file_to_load = argv[i+1];
            }
            if (arg == "-l") {
                freqLBASeq = argv[i+1];
            }
            if (arg == "-s") {
                calcInitial = 1;
            }
        }
    }
    if (!strcmp(argv[argc - 1], "-s")) {
        calcInitial = 1;
    }
    // Creates fstream objects which hold the contents of the
    // files input by the user.
    ifstream tracefile(file_to_load);
    ifstream LBAFile(freqLBASeq);

    // Returns an error message if one of the files does not exist.
    if (!tracefile) {
        cout << file_to_load << " does not seem to exist. " << endl;
        return 1;
    } else if (!LBAFile){
        cout << freqLBASeq << " does not seem to exist." << endl;
        return 1;
    } else { //Otherwise, loads the files into the traceset.
        TraceSet trace; // Creates a TraceSet object

        trace.readIn(tracefile);
        // If we only want the initial distance, then we just
        // calculate the initial distance and do nothing else.
        if (calcInitial){
            cout << "Initial: " << trace.total_seek_distance() << endl;
        } else {
            cout << "Initial: " << trace.total_seek_distance() << endl;
            trace.change_locations(trace.readLBAs(LBAFile), 0);
            cout << "Final: " <<  trace.total_seek_distance() << endl;
        }
    }
    return 0;
}
