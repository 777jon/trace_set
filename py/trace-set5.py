# trace-set.py
#
# A python script that calculates seek distance.
import sys

from tqdm import tqdm

INITIAL = 0
FINAL = 0

def main(trace, reorg):
    global INITIAL
    global FINAL
    traceL = fileToList(trace)
    reorgL = fileToList(reorg)
    # Initial
    initialBlockMap = mapBlocks(traceL, True)
    initialDistance = seekDistance(traceL, initialBlockMap)
    print 'Initial: ' + str(initialDistance)
    INITIAL = initialDistance
    fillMissingBlocks(traceL, reorgL)
    writeStats()

def fillMissingBlocks(traceL, reorgL):
    global FINAL
    print 'Filling in missing blocks...'
    reorgS = set(reorgL)
    m = max(max(traceL), max(reorgL))
    notInReorgL = [x for x in tqdm(xrange(0, m + 1, 8)) if not x in reorgS]
    # debug info
    print 'max: ' + str(m)
    print 'len: ' + str(len(notInReorgL))
    print 'first 10: ' + str(notInReorgL[:10])
    print 'last 10: ' + str(notInReorgL[len(notInReorgL) - 10:])
    #listToFile(notInReorgL, 'notInReorg')
    center = len(notInReorgL) / 2
    fullReorgL = notInReorgL[:center] + reorgL + notInReorgL[center:]
    #fullReorgL = reorgL + notInReorgL
    #listToFile(fullReorgL, 'fullReorg')
    # Final
    finalBlockMap = mapBlocks(fullReorgL)
    finalDistance = seekDistance(traceL, finalBlockMap)
    print 'Final: ' + str(finalDistance)
    FINAL = finalDistance

def seekDistance(traceL, blockMap):
    print 'Calculating distance...'
    lastBlock = traceL[0]
    totalDistance = 0
    largeDistanceL = []
    for index, block in enumerate(tqdm(traceL)):
        absDiff = abs(int(blockMap[block]) - int(blockMap[lastBlock]))
        if absDiff > 100000000:
            largeDistance = 'large distance jump: block ' + str(lastBlock) + \
                            ' to block ' + str(block) + ' with distance ' + \
                            str(absDiff) + 'at index ' + str(index)
            #print largeDistance
            largeDistanceL.append(largeDistance)
        totalDistance += 0 if absDiff == 0 else absDiff - 1
        lastBlock = block
    listToFile(largeDistanceL, 'largeDistances')
    return totalDistance

def mapBlocks(reorgL, initial = False):
    print 'Mapping blocks...'
    blockMap = {}
    s = 0
    for index, block in enumerate(tqdm(reorgL)):
        if block % 8 == 0 or block == 0:
            blockMap[block] = block / 8 if initial else index
        else:
            s += 1
            print 'err: ' + str(block) + ' is not a multiple of 8'
    if s > 0:
        sys.exit()
    return blockMap

def fileToList(filename):
    with open(filename, 'r') as f:
        return [int(i) for i in f.read().split('\n') if not i == '']

def listToFile(l, filename):
    print 'Writing ' + filename + '...'
    with open(filename, 'w') as output:
        for i in tqdm(l):
            output.write('%s\n' % i)

def writeStats():
    if INITIAL > FINAL:
        ratio = str(float(INITIAL) / FINAL) + 'x better'
    else:
        ratio = str(float(FINAL) / INITIAL) + 'x worse'
    stats = 'Initial: ' + str(INITIAL) + '\n' + \
            'Final: ' + str(FINAL) + '\n' + \
            'Ratio: ' + ratio + '\n' + \
            '% Change: ' + str(float(FINAL - INITIAL) / INITIAL * 100) + '\n'
    with open('trace-set-stats', 'w') as output:
        output.write(stats)

if __name__ == '__main__':
    if len(sys.argv) == 3:
        trace = sys.argv[1]
        reorg = sys.argv[2]
        main(trace, reorg)
    else:
        print 'Usage: trace-set.py <trace> <reorg>'
