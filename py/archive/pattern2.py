# pattern.py
#
# A python script that takes in a pattern and writes the distances of all
# patterns into one output file.
import sys
import subprocess

def all_traces(subtrace, trace_file, pattern, pattern2 = None):
    if pattern2 == None:
        storage = []
        for i in range(len(subtrace)):
            temp_trace  = subtrace[:i] + pattern + subtrace[i:]
            with open('python_reorg', 'w') as f:
                f.write('\n'.join(temp_trace))
            args = './trace-set2 -t ./' + trace_file + ' -l ./python_reorg'
            args = args.split(' ')
            popen = subprocess.Popen(args, stdout=subprocess.PIPE)
            output = popen.stdout.read()
            try:
                with open('records', 'a') as f:
                    f.write(output)
            except:
                with open('records', 'w') as f:
                    f.write(output)
            output = output.split(' ')
            output = output[-1]
            output = output[:-1]
            storage += [int(output)]
        x = min(storage)
        print "Minimum possible distance: " + str(x)
        with open("Optimal", 'w') as f:
            position = storage.index(x)
            final = subtrace[:position] + pattern + subtrace[position:]
            f.write(''.join(final))
            print "Optimal Pattern Placement saved."
    else:
        print "Dual Pattern placement"
        if any(i in subtrace for i in pattern) or any(i in subtrace for i in pattern2):
            return "SOMETHING'S OFF IN THE SUBTRACE"
        storage = []
        for i in range(len(subtrace)):
            storage.append([])
            print i
            for j in range(len(subtrace)):
                if i > j:
                    temp_trace = subtrace[:i] + pattern + subtrace[i:]
                    temp_trace = temp_trace[:j] + pattern2 + temp_trace[j:]
                elif i == j:
                    temp_trace = subtrace[:i] + pattern + pattern2 + subtrace[i:]
                else:
                    temp_trace = subtrace[:j] + pattern2 + subtrace[j:]
                    temp_trace = temp_trace[:i] + pattern + temp_trace[i:]
                with open('python_reorg', 'w') as f:
                    f.write('\n'.join(temp_trace))
                args = './trace-set2 -t ./' + trace_file + ' -l ./python_reorg'
                args = args.split(' ')
                popen = subprocess.Popen(args, stdout=subprocess.PIPE)
                output = popen.stdout.read()
                try:
                    with open('records', 'a') as f:
                        f.write(output)
                except:
                    with open('records', 'w') as f:
                        f.write(output)
                output = output.split(' ')
                output = output[-1]
                output = output[:-1]
                storage[i] += [int(output)]
        maxval = min(min(p) for p in storage)
        maxlist = next((subl for subl in storage if maxval in subl), None)
        if maxlist == None:
            print "SOMETHING WENT SCREWY BOSS"
            return
        print "Minimum Found Value: " + str(maxval)
        i = storage.index(maxlist)
        j = maxlist.index(maxval)
        with open("Optimal", 'w') as f:
            if i > j:
                temp_trace = subtrace[:i] + pattern + subtrace[i:]
                temp_trace = temp_trace[:j] + pattern2 + temp_trace[j:]
            elif i == j:
                temp_trace = subtrace[:i] + pattern + pattern2 + subtrace[i:]
            else:
                temp_trace = subtrace[:j] + pattern2 + subtrace[j:]
                temp_trace = temp_trace[:i] + pattern + temp_trace[i:]
            f.write('\n'.join(temp_trace))
            print "Optimal Pattern found."




def subtracer(trace, pattern):
    pattern = [int(i) for i in pattern]
    subtrace = [x for x in trace if int(x) not in pattern]
    return subtrace

def main(trace_file, pattern_file1, pattern_file2 = None, incomplete_reorg = None):
    with open(trace_file, 'r') as f:
        trace =  f.readlines()
    with open(pattern_file1, 'r') as f:
        pattern1 = f.readlines()
    if incomplete_reorg != None:
        with open(incomplete_reorg, 'r') as f:
            subtrace = f.readlines()
    else:
        subtrace = subtracer(trace, pattern1)
    if pattern_file2 != None:
        with open (pattern_file2, 'r') as f:
            pattern2 = f.readlines()
        subtrace = subtracer(subtrace, pattern2)
        all_traces(subtrace, trace_file, pattern1, pattern2)
    else:
        all_traces(subtrace, trace_file, pattern1)

if __name__ == '__main__':
    if len(sys.argv) == 4:
        trace_file = sys.argv[1]
        pattern_file = sys.argv[2]
        incomplete_reorg = sys.argv[3]
        main(trace_file, pattern_file, incomplete_reorg)
    elif len(sys.argv) == 3:
        trace_file = sys.argv[1]
        pattern_file = sys.argv[2]
        main(trace_file, pattern_file)
    else:
        print 'Usage: pattern.py <trace_file> <pattern_file>'