# pattern.py
#
# A python script that takes in a pattern and writes the distances of all
# patterns into one output file.
import sys
import subprocess

def all_traces(subtrace, trace_file, pattern):
    storage = []
    for i in range(len(subtrace)):
        temp_trace  = subtrace[:i] + pattern + subtrace[i:]
        with open('python_reorg', 'w') as f:
            f.write('\n'.join(temp_trace))
        args = './trace-set2 -t ./' + trace_file + ' -l ./python_reorg'
        args = args.split(' ')
        popen = subprocess.Popen(args, stdout=subprocess.PIPE)
        output = popen.stdout.read()
        writeOutput(output)
        output = output.split(' ')[-1][:-1]
        storage += [int(output)]
    writeOptimal(min(storage), subtrace, storage, pattern)

def writeOutput(output):
    try:
        with open('records', 'a') as f:
            f.write(output)
    except:
        with open('records', 'w') as f:
            f.write(output)

def writeOptimal(minimum, subtrace, storage, pattern):
    print "Minimum possible distance: " + str(minimum)
    with open("Optimal", 'w') as f:
        position = storage.index(minimum)
        final = subtrace[:position] + pattern + subtrace[position:]
        f.write(''.join(final))
        print "Optimal Pattern Placement saved."

def subtrace1(trace, pattern):
    subtrace = [x for x in trace if x not in pattern]
    return subtrace

def main(trace_file, pattern_file):
    with open(trace_file, 'r') as f:
        trace =  f.readlines()
    with open(pattern_file, 'r') as f:
        pattern = f.readlines()
    subtrace = subtrace1(trace, pattern)
    all_traces(subtrace, trace_file, pattern)

if __name__ == '__main__':
    if len(sys.argv) == 3:
        trace_file = sys.argv[1]
        pattern_file = sys.argv[2]
        main(trace_file, pattern_file)
    else:
        print 'Usage: pattern.py <trace_file> <pattern_file>'
