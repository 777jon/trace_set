# synthesize_data.py
#
# A Python script that organizes all the stats in trace_set/py/out into one table
import prettytable

TRACE_FOLDER = 'trace'
STATS_FILE = 'stats-'

def main():
    statsLL = []
    for i in xrange(1, 21):
        for j in xrange(1, 22):
            if i <= j:
                currentFile = TRACE_FOLDER + str(i) + '/' + STATS_FILE + \
                              str(i) + '-' + str(j)
                currentStatsL = readStats(currentFile)
                statsLL.append([str(i)] + [str(j)] + currentStatsL)
    writeTable(statsLL)
    writeCSV(statsLL)

def readStats(fileName):
    with open(fileName, 'r') as f:
        stats = []
        lineL = [i for i in f.read().split('\n')]
        for line in lineL:
            try:
                stats.append(line.split(':')[1].strip())
            except:
                continue
        return stats

def writeTable(statsLL):
    headerL = ['REORG', 'TRACE', 'INITIAL', 'FINAL', 'RATIO', '% CHANGE']
    table = prettytable.PrettyTable(headerL)
    for statsL in statsLL:
        table.add_row(statsL)
    writeFile('table', str(table))

def writeCSV(statsLL):
    data = []
    headerL = ['REORG', 'TRACE', 'INITIAL', 'FINAL', 'RATIO', '% CHANGE']
    data.append(','.join(headerL))
    for statsL in statsLL:
        data.append(','.join(statsL))
    writeFile('table.csv', '\n'.join(data))

def writeFile(fileName, data):
    with open(fileName, 'w') as out:
        out.write(data)

if __name__ == '__main__':
    main()
