#!/bin/bash

cd /research/geoff/summer2016/trace_set/py

for i in `seq 1 21`;
do
  for j in `seq $i 21`;
  do
    echo
    echo $i-$j
    echo
    python trace-set5.py ../../data/homes_traces/trace$j ../../dev/archive-jon/out/trace$i/reorg
    cp trace-set-stats out/trace$i/stats-$i-$j
  done
done
