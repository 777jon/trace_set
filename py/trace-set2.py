# trace-set.py
#
# A python script that calculates seek distance.
import sys

from tqdm import tqdm

def main(trace, reorg):
    traceL = fileToList(trace)
    reorgL = fileToList(reorg)
    # Initial
    initialBlockMap = mapBlocks(traceL, True)
    initialDistance = seekDistance(traceL, initialBlockMap)
    print 'Initial: ' + str(initialDistance)
    if list(set(traceL) - set(reorgL)) == []:
        finalBlockMap = mapBlocks(reorgL)
        finalDistance = seekDistance(traceL, finalBlockMap)
        print 'Final: ' + str(finalDistance)
    else:
        oneToOne(traceL, reorgL)

#def oneToOne(traceL, reorgL):
#    print 'Reorganization is not one to one. Finding missing blocks and ' + \
#          'rewriting reorg...'
#    #notInReorgL = list(set(traceL) - set(reorgL))
#    traceS = set(traceL)
#    reorgS = set(reorgL)
#    notInReorgL = [x for x in tqdm(traceS) if x not in reorgS]
#    notInReorgL.sort(key = lambda x: int(x))
#    listToFile(notInReorgL, 'notInReorg')
#    center = len(notInReorgL) / 2
#    first_half = notInReorgL[:center]
#    last_half = notInReorgL[center:]
#    fullReorgL = first_half + reorgL + last_half
#    listToFile(fullReorgL, 'fullReorg')
#    full_list = [int(i) for i in tqdm(fullReorgL)]
#    lastBlock = max(x)
#    all_blocks = range(0, lastBlock+1)
#    
#    # Final
#    #finalBlockMap = mapBlocks(fullReorgL)
#    #finalDistance = seekDistance(traceL, finalBlockMap)
#     print 'Final: ' + str(finalDistance)

def oneToOne(traceL, reorgL):
    print "Reorg is not one to one. Rearranging all blocks in order."
    reorgS = set(reorgL)
    traceS = set(traceL)
    last_block = max(set(map(int, reorgS|traceS)))
    print "Last Block: " + str(last_block)
    all_blocks = set(range(0, int(last_block)+1))
    notInReorgL = [str(x) for x in tqdm(all_blocks) if str(x) not in reorgS]
    #notInReorgL.sort(key = lambda x: int(x))
    listToFile(notInReorgL, 'notInReorg')
    center = len(notInReorgL) / 2
    first_half = notInReorgL[:center]
    last_half = notInReorgL[center:]
    fullReorgL = first_half + reorgL + last_half
    listToFile(fullReorgL, 'fullReorg')
    full_list = [str(i) for i in tqdm(fullReorgL)]
    finalBlockMap = mapBlocks(full_list)
    finalDistance = seekDistance(traceL, finalBlockMap)
    print 'Final: ' + str(finalDistance)

def seekDistance(traceL, blockMap):
    lastBlock = traceL[0]
    totalDistance = 0
    for block in traceL:
        absDiff = abs(int(blockMap[block]) - int(blockMap[lastBlock]))
        totalDistance += 0 if absDiff == 0 else absDiff - 1
        lastBlock = block
    return totalDistance

def mapBlocks(reorgL, initial = False):
    blockMap = {}
    for index, block in enumerate(reorgL):
        blockMap[block] = block if initial else index
    return blockMap

def fileToList(filename):
    with open(filename, 'r') as f:
        return [i for i in f.read().split('\n') if not i == '']

def listToFile(l, filename):
    with open(filename, 'w') as output:
        output.write('\n'.join(l) + '\n')

if __name__ == '__main__':
    if len(sys.argv) == 3:
        trace = sys.argv[1]
        reorg = sys.argv[2]
        main(trace, reorg)
    else:
        print 'Usage: trace-set.py <trace> <reorg>'
